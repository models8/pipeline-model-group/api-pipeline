# GRAFANA K6 TOOL - LOAD TESTING :


USAGE : 
------

DOCKER IMAGE : `grafana/k6:0.38.0`  (>12mo) :heart_eyes:

MAIN FILE: `./k6-load-test.js`

OUTPUT SUMMARY DIRECTORY : `./summary.html`

ENVIRONMENT VARIABLES : 
- `BASE_URL`
- `API_NAME`

COMMAND :

```bash
k6 run -e BASE_URL=$BASE_URL -e API_NAME=$API_NAME  k6-load-test.js
```

ADD MORE TESTS :
------


Install types for **intelliSense** in ci:
```bash
npm i
```
