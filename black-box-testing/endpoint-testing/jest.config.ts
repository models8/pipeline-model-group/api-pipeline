const testDir = "./testing"

export default {
  rootDir: testDir,
  testEnvironment: 'node',
  roots: [
    '<rootDir>/'
  ],
  preset: 'ts-jest',
  collectCoverage: false,
}
