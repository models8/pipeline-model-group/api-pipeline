import axios from "axios";

const APP_URL= process.env.APP_URL
const API_NAME = process.env.API_NAME

console.log(`\
APP URL : ${APP_URL}
APP NAME : ${API_NAME}\
`)


describe("test api endpoints",()=>{
  if(!APP_URL)
    throw new Error("Your should provide API_NAME env variable !")
  else if(!API_NAME)
    throw new Error("Your should provide API_NAME env variable !")

  const baseUrl = `${APP_URL}/${API_NAME}`
  console.log("API BASE URL : ",baseUrl)

  it('should /pipeline route work',async  () => {
    let status
    let data
    try {
      const res = await axios.get(`${baseUrl}/pipeline`);
      status = res.status
      data = res.data.text
    } catch (error : any) {
      status = error.response.status
    }
    expect(status).toBe(200)
    expect(data).toBe("Welcome in my beautiful Pipeline model !")
  });

  it('should /market route work',async  () => {
    let status
    let data
    try {
      const res = await axios
        .post(`${baseUrl}/market`,{name:"randmonName",capital: Math.random()*100})
      status = res.status
      data = res.data
    } catch (error : any) {
      status = error.response.status
    }
    expect(status).toBe(201)
    expect(data).toEqual({text : "market ajouté avec succès"})
  });

})
