# TAURUS TOOL - LOAD TESTING :


USAGE :
------

DOCKER IMAGE : `blazemeter/taurus:1.16.3`  (<1.35 GB) :poop:

MAIN FILE: `./taurus-load-testing.yml`

OUTPUT SUMMARY DIRECTORY : `./artifact/dashboard/index.html`

ENVIRONMENT VARIABLES :
- `BASE_URL`
- `API_NAME`

COMMAND :

```bash
bzt settings.env.BASE_URL=$BASE_URL settings.env.API_NAME=$API_NAME taurus-load-testing.yml
```

ADD MORE TESTS :
------
:exclamation: :hankey: the dashboard cannot be generated with more than one test in the suite
