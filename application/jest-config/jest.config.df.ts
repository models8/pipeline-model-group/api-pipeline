import * as path from "path";
const projectRoot = process.cwd()
const inteTestsDir = "white-box-testing/__tests__/dataflows"
export default {
  rootDir: path.resolve(projectRoot, inteTestsDir ),
  coverageProvider: 'v8',
  testEnvironment: 'node',
  // globalSetup : `<rootDir>/global-setup/setup.ts`,
  // globalTeardown : `<rootDir>/global-setup/teardown.ts`,
  roots: [
    `<rootDir>`
  ],
  preset: 'ts-jest',
  collectCoverage: false,
  modulePathIgnorePatterns: [
    "global-setup"
  ]
}
