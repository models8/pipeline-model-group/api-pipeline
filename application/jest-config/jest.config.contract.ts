import * as path from 'path'

const workDir = path.join(
  process.cwd(),
  'white-box-testing/__tests__',
  'contracts'
)
export default {
  rootDir: workDir,
  coverageProvider: 'v8',
  testEnvironment: 'node',
  roots: [`<rootDir>`],
  preset: 'ts-jest',
  collectCoverage: false,
  modulePathIgnorePatterns: ['global-setup']
}
