const outputDirPath = './outputs'
const unitReportDir = 'unit-report'
const coverageReportDir = 'unit-coverage'

export default {
  rootDir: '../',
  testEnvironment: 'node',
  roots: [
    '<rootDir>/src',
  ],
  preset: 'ts-jest',
  reporters: [
    'default',
    ['jest-junit', {
      addFileAttribute : true,
      suiteName: 'jest JUnit tests report',
      outputDirectory: `${outputDirPath}/${unitReportDir}`,
      outputName: 'junit.xml',
      titleTemplate: "{title}",
      classNameTemplate: '{classname}',
    }],
    ["./node_modules/jest-html-reporter", {
      pageTitle: "Your test suite",
      outputPath: `${outputDirPath}/${unitReportDir}/unit-report.html`,
      includeFailureMsg: true,
    }]
  ],
  collectCoverage: false,
  coverageProvider: 'v8',
  coverageDirectory :`${outputDirPath}/${coverageReportDir}`,
  coverageReporters: ['cobertura', 'html',"text-summary"],
  collectCoverageFrom: [
    '<rootDir>/src/core/**',
  ],
  coverageThreshold: {
    './src/core': {
      'branches': 60,
      'functions': 50,
      'lines': 50,
      'statements': 50
    }
  }
}
