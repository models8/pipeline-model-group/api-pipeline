import { disconnect, connect, connection } from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

class MongooseInMemory {
  static mongoServer: MongoMemoryServer
  static mongoUri: string
  static async createServer() {
    this.mongoServer = await MongoMemoryServer.create({})
    this.mongoUri = this.mongoServer.getUri()
    console.log('Server DB in Memory created at: ', this.mongoUri)
  }
  static async closeServer() {
    await disconnect()
    if (!MongooseInMemory.mongoServer)
      throw new Error('You try to close a not opened server !')
    await MongooseInMemory.mongoServer.stop()
    console.log('Mongo server closed')
  }

  public database: string = ''

  async connect(dbName: string = '') {
    if (!MongooseInMemory.mongoServer)
      throw new Error(
        'You should create in-memory server before to try open connection with mongo!'
      )
    await disconnect()
    const mongoUri = MongooseInMemory.mongoServer.getUri()
    if (!dbName)
      dbName = 'randomDb' + Math.floor(Math.random() * 10000000000).toString()
    this.database = dbName
    await connect(mongoUri, { dbName: dbName })
    console.log(`Connected to in memory database : ${mongoUri + this.database}`)
  }

  async disconnect() {
    await disconnect()
  }

  async removeAllCollections() {
    const collections = connection.collections
    for (const key in collections) await collections[key].deleteMany({})
  }
}

export { MongooseInMemory }
