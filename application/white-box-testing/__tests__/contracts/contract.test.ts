import { ExpressApp } from '../../../src/app'
import supertest = require('supertest')
import { MongooseInMemory } from '../../utils/MongooseInMemory'
import jestOpenAPI from 'jest-openapi'
import path from 'path'
import { mainRouter } from '../../../src/routes'

const app = new ExpressApp(mainRouter).init()
const request = supertest(app)
const mongoServer = new MongooseInMemory()

beforeAll(async () => {
  await MongooseInMemory.createServer()
  await mongoServer.connect()
})

beforeEach(async () => {
  await mongoServer.removeAllCollections()
})

afterAll(async () => {
  await mongoServer.disconnect()
  await MongooseInMemory.closeServer()
})

describe('"Markets" OpenAPI spec', () => {
  jestOpenAPI(path.join(process.cwd(), 'swagger.yml'))
  it('should ( GET /market ) satisfy OpenAPI spec', async () => {
    const res = await request.get(`/market`)
    expect(res).toSatisfyApiSpec()
  })

  it('should ( GET /market/{marketId} ) satisfy OpenAPI spec', async () => {
    const marketID = 2
    const res = await request.get(`/market/${marketID}`)
    expect(res).toSatisfyApiSpec()
  })

  it('should ( POST /market ) satisfy OpenAPI spec', async () => {
    const item = {
      name: 'darty',
      capital: 476546
    }

    // Make request (supertest used here)
    const res = await request.post(`/market`).send(item)
    expect(res).toSatisfyApiSpec()
  })
})
