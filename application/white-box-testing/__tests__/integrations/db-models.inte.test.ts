import { MongooseInMemory } from '../../utils/MongooseInMemory'
import { modelMarket } from '../../../src/database/model/market.model'
import { IMarket } from '../../../src/core/market/market.interface'

const mongoServer = new MongooseInMemory()

beforeAll(async () => {
  await MongooseInMemory.createServer()
  await mongoServer.connect()
})

beforeEach(async () => {
  await mongoServer.removeAllCollections()
})

afterAll(async () => {
  await mongoServer.disconnect()
  await MongooseInMemory.closeServer()
})

describe('test my api', () => {
  it('should work', async () => {
    const random = Math.floor(Math.random() * 100)
    const name = 'darty' + random
    const market: IMarket = {
      name,
      capital: random
    }
    await new modelMarket(market).save()
    const got = await modelMarket.findOne({ name })
    expect(got?.name).toBe(name)
  })

  it('should work v2', async () => {
    const random = Math.floor(Math.random() * 100)
    const name = 'fnac' + random
    const market: IMarket = {
      name,
      capital: random
    }
    await new modelMarket(market).save()
    const got = await modelMarket.findOne({ name })
    expect(got?.name).toBe(name)
  })
})
