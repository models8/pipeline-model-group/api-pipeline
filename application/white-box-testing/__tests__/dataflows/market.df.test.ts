import supertest = require('supertest')
import { modelMarket } from '../../../src/database/model/market.model'
import { IMarket } from '../../../src/core/market/market.interface'
import { MongooseInMemory } from '../../utils/MongooseInMemory'
import { ExpressApp } from '../../../src/app'
import { mainRouter } from '../../../src/routes'

const app = new ExpressApp(mainRouter).init()
const request = supertest(app)

const mongoServer = new MongooseInMemory()

beforeAll(async () => {
  await MongooseInMemory.createServer()
  await mongoServer.connect()
})

beforeEach(async () => {
  await mongoServer.removeAllCollections()
})

afterAll(async () => {
  await mongoServer.disconnect()
  await MongooseInMemory.closeServer()
})

describe('Test /market dataflow', () => {
  const item = {
    name: 'darty',
    capital: 476546
  }

  it(`should receive success`, async () => {
    const res = await request.post(`/market`).send(item)

    expect(res.body).toEqual({ text: 'market ajouté avec succès' })
    expect(res.status).toBe(201)
  })

  it(`should database contain new market`, async () => {
    await request.post(`/market`).send(item)

    const market: IMarket = await modelMarket
      .findOne({ name: item.name })
      .lean()
    expect(market).not.toBe(null)
    expect(market.name).toBe(item.name)
  })
})
