module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['tsconfig.build.json', 'tsconfig.specs.json'],
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint/eslint-plugin'],
  extends: [
    'plugin:import/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended'
  ],
  env: {
    node: true,
    jest: true
  },
  ignorePatterns: ['.eslintrc.js', 'swagger*'],
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    /*--------My Owner rules ----------------*/
    '@typescript-eslint/no-inferrable-types': 0,
    'import/no-unresolved': 0,
    'import/no-cycle': 1,
    'import/prefer-default-export': 0,
    '@typescript-eslint/semi': [2, 'never'],
    'max-len': [
      2,
      120,
      {
        ignoreComments: true,
        ignoreStrings: true
      }
    ],
    'no-unexpected-multiline': 2,
    'no-underscore-dangle': 0,
    'no-continue': 0
  }
}
