export const greaterThanTen = (
  text: string
): { text: string; isGreater: boolean } => {
  if (text.length < 10) return { text, isGreater: false }
  else return { text, isGreater: true }
}
//Hey
