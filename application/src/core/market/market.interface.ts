export interface IMarket {
  name: string
  capital: number
  createdAt?: Date
}
