import { greaterThanTen } from './market'

describe("test 'greaterThanThen' function", () => {
  it('should result be true', () => {
    const text = 'hey friends !'
    const result = greaterThanTen(text)
    expect(result.isGreater).toBe(true)
  })

  it('should result be false', () => {
    const text = 'hey !!'
    const result = greaterThanTen(text)
    expect(result.isGreater).toBe(false)
  })
})
