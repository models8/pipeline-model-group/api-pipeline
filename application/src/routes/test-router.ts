import { Router } from 'express'

const router = Router()

router.get(`/test`, async (req, res) => {
  res.status(200).json({ text: 'This is TEST path' })
})

export { router as testRouter }
