import { Router } from 'express'
import { modelMarket } from '../database/model/market.model'

const router = Router()

router.get(`/market`, async (req, res) => {
  try {
    res.status(200).json([
      { name: 'SuperU', capital: 1065478 },
      { name: 'amazon', capital: 867687687687 }
    ])
  } catch (err: any) {
    res
      .status(400)
      .json({ oops: 'Oops ! you made a bad request', err: err.message })
  }
})

router.post(`/market`, async (req, res) => {
  try {
    const market = new modelMarket(req.body)
    await market.save()
    res.status(201).json({ text: 'market ajouté avec succès' })
  } catch (err: any) {
    res
      .status(400)
      .json({ oops: 'Oops ! you made a bad request', err: err.message })
  }
})

router.get(`/market/:marketId`, async (req, res) => {
  try {
    const marketId = req.params.marketId
    if (!marketId) throw new Error('You should provide a market ID')
    res.status(200).json({ name: 'zara', capital: 687868 })
  } catch (err: any) {
    res
      .status(400)
      .json({ oops: 'Oops ! you made a bad request', err: err.message })
  }
})

export { router as marketRouter }
