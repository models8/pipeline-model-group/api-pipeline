import { Router } from 'express'

const router = Router()

router.get(`/secret`, async (req, res) => {
  res.status(200).json({ text: 'I am your father !!' })
})

export { router as secretRouter }
