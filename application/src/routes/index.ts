import { Router } from 'express'
import { marketRouter } from './market-router'
import { pipelineRouter } from './pipeline-router'
import { secretRouter } from './secret-router'
import { testRouter } from './test-router'

export const mainRouter = Router()

mainRouter.get(`/`, async (req, res) => {
  res.status(200).json({ text: 'This is root boy !' })
})

mainRouter.use(pipelineRouter)
mainRouter.use(marketRouter)
mainRouter.use(secretRouter)
mainRouter.use(testRouter)
