import { Router } from 'express'

const router = Router()

router.get(`/pipeline`, async (req, res) => {
  res.status(200).json({ text: 'Welcome in my beautiful Pipeline model !' })
})

export { router as pipelineRouter }
