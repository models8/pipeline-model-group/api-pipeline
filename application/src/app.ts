import 'reflect-metadata'
import express, { Express } from 'express'
import { Router } from 'express/ts4.0'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import * as fs from 'fs'
import path from 'path'

class ExpressApp {
  private app = express()

  constructor(private router: Router, private apiName: string = '') {}

  private setHeaders() {
    this.app.use((req, res, next) => {
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Origian, X-Requested-With, Content, Accept, Content-Type, Authorization'
      )
      res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, PUT, DELETE, PATCH, OPTIONS'
      )
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Accept', 'application/json')
      next()
    })
  }

  private setParsers() {
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(bodyParser.json())
    this.app.use(cookieParser())
  }

  private async setOpenAPI(): Promise<void> {
    console.log('-----------SWAGGER INIT-----------')
    const swaggerUi = await import('swagger-ui-express')
    const { load } = await import('js-yaml')
    const ymlBuffer = fs.readFileSync(path.join(process.cwd(), 'swagger.yml'))
    const swaggerDocument: any = load(ymlBuffer.toString())
    this.router.use(
      `/api-docs`,
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument)
    )
  }

  public init(): Express {
    if (process.env.NODE_ENV === 'development') {
      this.setOpenAPI().then()
    }
    this.setHeaders()
    this.setParsers()
    this.app.use(`/${this.apiName}`, this.router)
    return this.app
  }
}

export { ExpressApp }
