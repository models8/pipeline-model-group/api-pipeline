import { Document, model, Schema } from 'mongoose'
import { IMarket } from '../../core/market/market.interface'

interface IMarketDoc extends Document, IMarket {}

const schema = new Schema({
  name: { type: String, required: 'Vous devez entrer le nom ' },
  capital: { type: Number, required: 'Vous devez entrer le capital' }
})

export const modelMarket = model<IMarketDoc>('markets', schema)
