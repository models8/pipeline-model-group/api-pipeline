import { connect, disconnect } from 'mongoose'
import { IMongoConnect } from './IMongoConnect'

export async function dbConnexion(dbconnexion: IMongoConnect) {
  await disconnect()
  const {
    hostname,
    dbName,
    user = '',
    pwd = '',
    port = 27017,
    authSource = '',
    dnsSrvRecord = ''
  } = dbconnexion

  const { NODE_ENV } = process.env

  const url = (): string => {
    if (!hostname)
      throw new Error('You should give a mongo hostname for connexion')
    else if (!dbName)
      throw new Error('You should give a mongo database connexion')

    const protocol = dnsSrvRecord ? 'mongodb+srv' : 'mongodb'
    const auth = user && pwd ? `${user}:${pwd}@` : ''
    const host = `${hostname}:${port}`
    return `${protocol}://${auth}${host}/${dbName}`
  }

  const params = (): string => {
    const params: string[] = []

    if (authSource) params.push(`authSource=${authSource}`)

    if (NODE_ENV === 'production') {
      params.push('retryWrites=true')
      params.push('w=majority')
    }

    return `?${params.join('&')}`
  }

  const uri = `${url()}${params()}`

  try {
    await connect(uri)
    console.log('Ok, DB MONGO EST CONNECTE SUR : ', uri)
  } catch (err) {
    throw new Error(`IL Y A UNE UNE ERREUR DE BASE DE DONNE !! :
      - L'url DB : ${uri}
      - L'erreur est : ${err}`)
  }
}
