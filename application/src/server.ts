import { dbConnexion } from './database/MongoConnect/mongoConnect'
import { IMongoConnect } from './database/MongoConnect/IMongoConnect'
import { ExpressApp } from './app'
import { mainRouter } from './routes'

const run = async () => {
  const { API_PORT } = process.env
  const dbInfos: IMongoConnect = {
    hostname: process.env.MONGO_HOSTNAME as string,
    port: process.env.MONGO_PORT as string,
    dbName: process.env.MONGO_API_DB as string,
    authSource: process.env.MONGO_AUTH_SOURCE as string,
    user: process.env.MONGO_API_DB_USERNAME as string,
    pwd: process.env.MONGO_API_DB_PASSWORD as string
  }

  try {
    await dbConnexion(dbInfos)
    console.log('okokok')
  } catch (err) {
    console.log('nononononono \n', err)
  }
  const API_NAME = process.env.API_NAME as string
  const app = new ExpressApp(mainRouter, API_NAME).init()
  app.listen(API_PORT, () => {
    console.log(`Mon node js ecoute sur le port : "${API_PORT as string}" `)
    console.log(`Le nom de l'api est ${API_NAME}`)
  })
}

run()
  .then(() => ({}))
  .catch(() => ({}))
