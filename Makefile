DOCKER_IMAGE = gittools/gitversion:5.10.1-alpine.3.14-6.0
CURRENT_DIR = $$(pwd)
CONTAINER_DIR = /repo
COMMIT_REF_NAME = master

init:
	npm i
	npm --prefix ./application install

gitver:
	docker run --rm -v "$(CURRENT_DIR):$(CONTAINER_DIR)" $(DOCKER_IMAGE) $(CONTAINER_DIR) | grep "FullSemVer"
gitver-full:
	docker run --rm -v "$(CURRENT_DIR):$(CONTAINER_DIR)" $(DOCKER_IMAGE) $(CONTAINER_DIR)
